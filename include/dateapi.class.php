<?php
/**
 * 精简节假日API
 * @copyright tool.bitefu.net
 * @author xiaogg@sina.cn
 */
class DateApi
{
    // 数据存储路径
    private $dataPath = 'data/';
    // 缓存数组
    private $cache = array();
    /**
     * 获取一天的节假日情况
     * @param string $day 日期如 20250101 或 2025-01-01
     * @param int $type 类型 1工资倍数【0：工作日 1：周未双倍工资 2：假日三倍工资】,2具体类型【1 工作日 2调休(工作日) 3周未 4假日】
     * @return array 返回结果数组
     */
    public function getday($day, $type = 1)
    {
        // 验证日期格式
        if (!$this->validateDate($day)) {
            return array('status' => 0, 'info' => '日期格式错误');
        }
        // 清理日期格式
        $day = str_replace('-', '', $day);
        // 获取年份
        $year = substr($day, 0, 4);
        // 获取年份数据
        $yearData = $this->getYearData($year);
        if ($yearData === false) {
            return array('status' => 0, 'info' => '数据获取失败');
        }
        // 获取节假日信息
        $holidayData = $this->getHolidayInfo($day, $yearData);
        // 根据类型返回结果
        if ($type == 2) {
            $data = $this->getDetailType($day, $holidayData);
        } else {
            $data = $this->getWageMultiplier($day, $holidayData);
        }
        return array('status' => 1, 'info' => $data);
    }
    /**
     * 检查是否是周末
     * @param string $day 日期
     * @return int 1是周末，0不是
     */
    private function checkWeekend($day)
    {
        $weekday = date('N', strtotime($day));
        return in_array($weekday, array(6, 7)) ? 1 : 0;
    }
    /**
     * 验证日期格式
     * @param string $date 日期
     * @return bool 是否有效
     */
    public function validateDate($date,$format=null) {
        if(empty($date) || strlen($date)<8)return false;
        // 尝试创建 DateTime 对象
        if(empty($format) && is_numeric($date))$format='Ymd';
        if ($format) {
            // 使用指定格式验证
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) === $date;
        } else {
            // 使用 strtotime 验证
            return (strtotime($date) !== false);
        }
    }
    /**
     * 获取一年的数据
     * @param string $year 年份
     * @return array|false 年份数据或false
     */
    private function getYearData($year)
    {
        // 检查缓存
        if (isset($this->cache[$year])) {
            return $this->cache[$year];
        }
        // 本地文件路径
        $filePath = $this->dataPath . $year . '_data.json';
        // 检查本地文件是否存在
        if (file_exists($filePath)) {
            $data = json_decode(file_get_contents($filePath), true);
            if ($data !== null) {
                $this->cache[$year] = $data;
                return $data;
            }
        }
        // 远程获取数据
        $url = 'https://gitee.com/web/holidays_api/raw/master/data/' . $year . '_data.json';
        $content = @file_get_contents($url);
        if ($content !== false) {
            $data = json_decode($content, true);
            if ($data !== null) {
                // 保存到本地
                file_put_contents($filePath, json_encode($data));
                $this->cache[$year] = $data;
                return $data;
            }
        }
        return false;
    }
    /**
     * 获取节假日信息
     * @param string $day 日期
     * @param array $yearData 年份数据
     * @return int|null 节假日信息
     */
    private function getHolidayInfo($day, $yearData)
    {
        if ($yearData) {
            $monthDay = substr($day, 4, 4);
            if (isset($yearData[$monthDay]) && is_numeric($yearData[$monthDay])) {
                return $yearData[$monthDay];
            }
        }
        return null;
    }
    /**
     * 获取详细类型
     * @param string $day 日期
     * @param mixed $holidayData 节假日数据
     * @return int 类型
     */
    private function getDetailType($day, $holidayData)
    {
        if (is_numeric($holidayData)) {
            return $holidayData > 0 ? 4 : 2;
        }
        return $this->checkWeekend($day) ? 3 : 1;
    }
    /**
     * 获取工资倍数
     * @param string $day 日期
     * @param mixed $holidayData 节假日数据
     * @return int 工资倍数
     */
    private function getWageMultiplier($day, $holidayData)
    {
        return is_numeric($holidayData) ? $holidayData : $this->checkWeekend($day);
    }
}
?>